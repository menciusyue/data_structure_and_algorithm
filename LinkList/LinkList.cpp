/*
一个完整的链表包括:
1. 头插法
2. 尾插法
3. 头部增加一个元素
4. 尾部增加一个元素
5. 删除一个指定元素
6. 将第一个出现的元素的特定的值修改成指定数据
7. 打印
*/
#include <iostream>
using namespace std;

//头结点的定义
struct Node
{
	int data;
	Node* next = NULL;	
	Node(int value):data(value){this->data = value;};	//快速初始化
	Node(){};		//无参生成结点
};

//尾插法初始化链表
Node* RearInsert(Node* head,int arr[],int arrSize){
	Node* rear = head;
	rear->next = NULL;
	for(int i =0;i < arrSize;i++){
		Node* new_node = new Node(arr[i]);
		new_node->next = rear->next;
		rear->next = new_node;
		rear = new_node;
	}
	return head;
}

//用头插法初始化链表
Node* HeadInsert(Node* head,int arr[],int arrSize){
	head = NULL;
	for(int i = 0;i < arrSize;i++){
		Node* new_node = new Node(arr[i]);
		new_node->next = head;
		head = new_node;
	}
	Node* new_head = new Node;		//为了使头结点都不存放数据，打印时也好统一
	new_head->next = head;
	return new_head;
}

//头部增加一个元素e
Node* HeadAdd(Node* head,int e){
	Node* new_node = new Node(e);
	new_node->next = head->next;
	head->next = new_node;
	return head;
}

//尾部增加一个元素e
Node* RearAdd(Node* head,int e){
	//需要先找到最后一个结点
	Node* end = head;			//寻找最后一个结点
	while(end->next != NULL){
		end = end->next;
	}
	Node* new_node = new Node(e);
	new_node->next = NULL;
	end->next = new_node;
	return head;
}

//删除第一个出现的值为e的结点
Node* Delete(Node* head,int e){
	Node* target_prior = head;
	while(target_prior->next->data != e){
		target_prior = target_prior->next;
	}
	Node* target = target_prior->next;
	target_prior->next = target->next;
	delete target;
	return head;
}

//将第一个值为e的结点的值修改为num
Node* Change(Node* head,int e,int num){
	Node* cur = head->next;
	while(cur != NULL){
		if(cur->data == e){
			cur->data = num;
			break;
		}
		cur = cur->next;
	}
	return head;
}

//打印链表
void Print(Node* head){
	Node* cur = head->next;
	while(cur != NULL){
		cout<<cur->data<<"  ";
		cur = cur->next;
	}
	cout<<endl;
}

int main(int argc, char const *argv[])
{
	int arr[5] = {1,2,3,4,5};
	Node* head = new Node();
	head = RearInsert(head,arr,5);
	//Print(HeadInsert(head,arr,5));		头插法  5,4,3,2,1
	//Print(RearInsert(head,arr,5));		尾插法  1,2,3,4,5  
	//Print(HeadAdd(head,0));				头部添加元素		0,1,2,3,4,5
	//Print(RearAdd(head,6));				尾部添加元素		1,2,3,4,5,6
	//Print(Delete(head,1));				删除一个元素 		2,3,4,5
	//Print(Change(head,5,100));			改变指定元素的值 	1,2,3,4,100
	return 0;
}