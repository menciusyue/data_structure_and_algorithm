#ifndef _QUEUE_H
#define _QUEUE_H 

template <class T>
typedef struct Node
{
	T elem;
	Node<T>* next;
	Node(T val):elem(val){};
};
template <class T>
class Queue
{
	Node<T>* front;
	Node<T>* rear;
public:
	Queue():front(NULL),rear(NULL){};
	~Queue();
	void IsEmpty();
	void Enqueue(T );
	Node<T>* TopVal();
	void DeQueue();
};
template <class T>
void Queue<T>::Enqueue(Node<T>* t){

}

#endif
