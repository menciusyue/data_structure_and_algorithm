#ifndef _TREE_H
#define _TREE_H 
template <class T>
struct Tnode{
	T data;
	Tnode<T>* left;
	Tnode<T>* right;
	Tnode(T val):data(val),left(NULL),right(NULL){};
	
	

};
template <class T>
class Tree
{
	Tnode<T>* tnode;
public:
	Tree():tnode(NULL){};
	~Tree();
	void DeleteTNode(Tnode<T>* t){delete t;t = NULL;};
	void Add(T val,Tnode<T>* t);
	void Print(Tnode<T>* tn);
};
template <class T>
Tree<T>::~Tree(){
	if(this){
		DeleteTNode(this->left);
		DeleteTNode(this->right);
	}
}
template <class T>
void Tree<T>::Add(T val,Tnode<T>* tnode){
	if(tnode == NULL){
		tnode = new Tnode<T>(val);
	}
	else{
		if(tnode->data < val){
			if(tnode->right == NULL){
				Tnode<T>* tmp = new Tnode<T>(val);
				tnode->right = tmp;
			}
			else{
				Add(val,tnode->right);
			}
		}
		else{
			if(tnode->left == NULL){
				Tnode<T>* tmp = new Tnode<T>(val);
				tnode->left = tmp;
			}
			else{
				Add(val,tnode->left);
			}
		}
	}
}
template <class T>
void Tree<T>::Print(Tnode<T>* tn){
	if(tn){
		std::cout<<tn->data<<std::endl;
		Print(tn->left);
		Print(tn->right);
	}
}
#endif