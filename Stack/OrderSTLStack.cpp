#include <iostream>
#include <stack>
using namespace std;
struct Node
{
	int data;
	Node* left;
	Node* right;
	Node(int val):data(val),left(NULL),right(NULL){};
};

struct Tree
{
	Node* tnode;
	void Add(int val,Node* t){
		if(t == NULL){
			t = new Node(val);
		}
		else{
			if(t->data > val){
				if(t->left == NULL){
					Node* tmp = new Node(val);
					t->left = tmp;
				}
				else{
					Add(val,t->left);
				}
			}
			else{
				if(t->right == NULL){
					Node* tmp = new Node(val);
					t = tmp;
				}
				else{
					Add(val,t->right);
				}
			}
		}
	};

};

void Print(Node* t){
	if(t){
		cout<<t->data<<endl;
		Print(t->left);
		Print(t->right);
	}
}
int main(int argc, char const *argv[])
{
	int arr[10] = {5,6,0,9,3,8,1,2,7,4};
	Tree* tree = NULL;
	Node* root = NULL;
	for(int i = 0;i < 10;i++){
		if(i == 0){
			root = new Node(arr[i]);
			tree->tnode = root;
		}
		tree->Add(arr[i],root);
	}
	//Print(root);
	return 0;
}
