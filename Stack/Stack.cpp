//看似简单的链栈调了半天，还是要多练呀。。。。抓紧时间冲击最后一波了
//链栈一般存储空间都够的，我就不写栈满的函数了，想写还得都改一遍，懒得弄了
#include <iostream>
using namespace std;
template <class T>
class Node		//class默认是private，struct默认是public
{
public:
	T data;
	Node<T>* next;
	Node(T val):data(val),next(NULL){};
	//~Node(){};	这边析构根本不需要，以后老老实实用结构体就不会有这么多破事了
	
};
template<class T>
class Stack
{
	Node<T>* top;
public:
	Stack():top(NULL){};
	void Pop();
	void Push(T val);
	bool IsEmpty();
	T TopVal();
	~Stack(){delete top;top = NULL;};
	
};
template<class T>
void Stack<T>::Push(T val){			//空栈和栈中有元素是不一样的
	if(top == NULL){
		top = new Node<int>(val);
	}
	else{
		Node<T>* tmp = new Node<T>(val);
		tmp->next = top;
		top = tmp;
	}
}
template <class T>
void Stack<T>::Pop(){
	if(top == NULL){
		cout<<"Stack is empty!"<<endl;
		return ; 
	}
	else{
		Node<T>* tmp = top;
		top = top->next;
		delete tmp;
		tmp = NULL;
	}
}
template <class T>
bool Stack<T>::IsEmpty(){
	return top == NULL;
}
template <class T>
T Stack<T>::TopVal(){
	return top->data;
}
int main(int argc, char const *argv[])
{
	Stack<int>* s = new Stack<int>();
	for(int i = 0;i < 10;i++){		
		s->Push(i);				//都是指针形式
	}
	for(int i = 0;i < 10;i++){
		cout<<s->TopVal()<<endl;
		s->Pop();
	}
	return 0;
}
